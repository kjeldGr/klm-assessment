//
//  KGRouter.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

protocol KGRouter {
    func makeFirstViewController() -> UIViewController?
}
