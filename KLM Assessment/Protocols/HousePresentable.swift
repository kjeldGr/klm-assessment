//
//  HousePresentable.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

protocol HousePresentable {
    var displayName: String { get }
    var number: Int { get }
    var amount: Int { get }
    var image: UIImage? { get }
    var detailImages: [UIImage]? { get }
    func increaseAmount()
    func decreaseAmount()
}
