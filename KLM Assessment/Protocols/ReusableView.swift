//
//  ReusableView.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
