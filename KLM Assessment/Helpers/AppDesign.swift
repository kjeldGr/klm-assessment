//
//  AppDesign.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

struct AppDesign {
    
    // MARK: - Public enums
    
    enum Color {
        case navigationBar
        case segmentedControlBackground
        case houseNumber
        case border
    }
    
    // MARK: - Private enums
    
    private enum AppColor: Int {
        case gray = 0xA9AAAA
        case lightBlue = 0x56ABDE
        case orange = 0xD57837
    }
    
    // MARK: - Public methods
    
    // MARK: Static
    
    static func color(_ color: Color) -> UIColor {
        switch color {
        case .navigationBar,
             .segmentedControlBackground:
            return UIColor(hex: AppColor.lightBlue.rawValue)
        case .houseNumber:
            return UIColor(hex: AppColor.orange.rawValue)
        case .border:
            return UIColor(hex: AppColor.gray.rawValue)
        }
    }
    
}
