//
//  StringExtensions.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

extension String {
    
    static func localize(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
}
