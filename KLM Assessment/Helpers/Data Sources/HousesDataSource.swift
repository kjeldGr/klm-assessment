//
//  HousesDataSource.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

enum HouseDisplayStyle {
    case list, grid
}

class HousesDataSource: NSObject, UICollectionViewDataSource {
    
    // MARK: - Public properties
    
    var displayStyle: HouseDisplayStyle = .grid
    
    // MARK: - Private properties
    
    private var presenter: HousesViewPresenter
    
    // MARK: - Initializers
    
    init(presenter: HousesViewPresenter) {
        self.presenter = presenter
    }
    
    // MARK: - Public methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfHouses
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = displayStyle == .grid ? HouseSmallCell.reuseIdentifier : HouseLargeCell.reuseIdentifier
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        if let houseCell = cell as? HouseCell,
            let house = presenter.house(at: indexPath.item) {
            houseCell.present(with: house)
        }
        return cell
    }
    
}
