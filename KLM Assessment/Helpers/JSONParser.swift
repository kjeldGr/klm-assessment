//
//  JSONParser.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

class JSONParser {
    
    class func jsonObjectFrom<JSONObject>(fileName: String) -> JSONObject? {
        guard let jsonPath = Bundle.main.url(forResource: fileName, withExtension: "json") else {
            assertionFailure("We need \(fileName).json to continue test")
            return nil
        }
        
        do {
            let jsonData = try Data(contentsOf: jsonPath, options: Data.ReadingOptions.uncached)
            guard let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? JSONObject else {
                assertionFailure("Something is wrong with the json file")
                return nil
            }
            return jsonObject
        } catch {}
        return nil
    }
    
}
