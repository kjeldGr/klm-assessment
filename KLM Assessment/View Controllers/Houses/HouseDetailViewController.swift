//
//  HouseDetailViewController.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class HouseDetailViewController: UIViewController {

    // MARK: - Public properties
    
    var presenter: HouseDetailViewPresenter!
    var index: Int!
    
    // MARK: IBOutlets
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Public methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func showInfo() {
        imageView.isHidden = !imageView.isHidden
    }
    
    // MARK: - Private methods
    
    private func setupView() {
        descriptionLabel.text = presenter.displayDescription
        nameLabel.text = presenter.displayName
        addressLabel.text = presenter.displayAddress
        numberLabel.text = presenter.displayNumber
        imageView.image = presenter.displayImage
    }
    
}
