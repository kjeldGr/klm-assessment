//
//  HousesPagingViewController.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import SnapKit

class HousesPagingViewController: UIViewController {

    // MARK: - Public properties
    
    var presenter: HousesViewPresenter!
    var selectedIndex: Int!
    
    // MARK: - Private properties
    
    private let bottomBarHeight: CGFloat = 53
    fileprivate lazy var pageViewController: UIPageViewController = self.makePageViewController()
    fileprivate lazy var bottomBar: DetailBottomBar = self.makeDetailBottomBar()
    
    // MARK: - Public methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Private methods
    
    fileprivate func houseDetailViewControllerWithHouseAtIndex(_ index: Int) -> HouseDetailViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let house = presenter.house(at: index),
              let houseDetailViewController = storyboard.instantiateViewController(withIdentifier: "HouseDetailViewController") as? HouseDetailViewController else {
                return nil
        }
        let houseDetailViewPresenter = HouseDetailViewPresenter(house: house)
        houseDetailViewController.presenter = houseDetailViewPresenter
        houseDetailViewController.index = index
        return houseDetailViewController
    }
    
    // MARK: UI
    
    private func setupView() {
        setupPageViewController()
        setupBottomBar()
    }
    
    private func setupPageViewController() {
        view.addSubview(pageViewController.view)
        pageViewController.view.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
    
    private func setupBottomBar() {
        view.addSubview(bottomBar)
        bottomBar.snp.makeConstraints { make in
            make.top.equalTo(pageViewController.view.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(bottomBarHeight)
        }
    }
    
    // MARK: Factory methods
    
    private func makePageViewController() -> UIPageViewController {
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        if let viewController = houseDetailViewControllerWithHouseAtIndex(selectedIndex) {
            pageViewController.setViewControllers([viewController], direction: .forward, animated: false, completion: nil)
        }
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        return pageViewController
    }
    
    private func makeDetailBottomBar() -> DetailBottomBar {
        let bottomBar = DetailBottomBar()
        bottomBar.translatesAutoresizingMaskIntoConstraints = false
        bottomBar.delegate = self
        if let house = presenter.house(at: selectedIndex) {
            bottomBar.layout(with: house)
        }
        return bottomBar
    }

}

// MARK: - UIPageViewControllerDelegate

extension HousesPagingViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let houseDetailViewController = pageViewController.viewControllers?.first as? HouseDetailViewController,
              let house = presenter.house(at: houseDetailViewController.index) else { return }
        bottomBar.layout(with: house)
    }
    
}

// MARK: - UIPageViewControllerDataSource

extension HousesPagingViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let previousViewController = viewController as? HouseDetailViewController else { return nil }
        return houseDetailViewControllerWithHouseAtIndex(previousViewController.index - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let previousViewController = viewController as? HouseDetailViewController else { return nil }
        return houseDetailViewControllerWithHouseAtIndex(previousViewController.index + 1)
    }
    
}

// MARK: DetailBottomBarDelegate

extension HousesPagingViewController: DetailBottomBarDelegate {
    
    func infoButtonPressed() {
        guard let houseDetailViewController = pageViewController.viewControllers?.first as? HouseDetailViewController else { return }
        houseDetailViewController.showInfo()
    }
    
}
