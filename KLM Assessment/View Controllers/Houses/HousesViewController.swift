//
//  HousesViewController.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit
import SnapKit

class HousesViewController: UIViewController {

    // MARK: - Public properties
    
    var presenter: HousesViewPresenter!
    var selectedHouse: ((House, HousesViewPresenter) -> Void)?
    
    // MARK: - Private properties
    
    private let segmentedControlViewHeight: CGFloat = 53
    
    // MARK: Views
    
    private lazy var collectionViewDataSource: HousesDataSource = self.makeCollectionViewDataSource()
    private lazy var collectionView: UICollectionView = self.makeCollectionView()
    private lazy var flowLayout: UICollectionViewFlowLayout = self.makeFlowLayout()
    private lazy var segmentedControlView: SegmentedControlView = self.makeSegmentedControlView()
    
    // MARK: - Public methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = String.localize("view_title_houses")
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        segmentedControlView.selectItem(at: 0)
        reloadCollectionView(category: .all)
    }

    // MARK: - Private methods
    
    fileprivate func reloadCollectionView(category: HouseCategory) {
        presenter.filterType = category
        collectionView.reloadData()
    }
    
    // MARK: UI methods
    
    private func setupView() {
        setupCollectionView()
        setupSegmentedControlView()
    }
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
    
    private func setupSegmentedControlView() {
        view.addSubview(segmentedControlView)
        segmentedControlView.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(segmentedControlViewHeight)
        }
    }
    
    // MARK: Factory methods
    
    private func makeCollectionViewDataSource() -> HousesDataSource {
        let dataSource = HousesDataSource(presenter: presenter)
        return dataSource
    }
    
    private func makeCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = self
        collectionView.register(HouseLargeCell.self, forCellWithReuseIdentifier: HouseLargeCell.reuseIdentifier)
        collectionView.register(HouseSmallCell.self, forCellWithReuseIdentifier: HouseSmallCell.reuseIdentifier)
        return collectionView
    }
    
    private func makeFlowLayout() -> UICollectionViewFlowLayout {
        // Layout values
        let cellsPerRow = CGFloat(4)
        let sectionInset = CGFloat(5)
        let cellSpacing = CGFloat(5)
        // Calculate itemSize
        let itemWidthAndHeight = (UIScreen.main.bounds.width - (sectionInset * 2.0) - ((cellsPerRow - 1.0) * cellSpacing)) / cellsPerRow
        // Create flow layout
        let flowLayout = UICollectionViewFlowLayout()
        // Set item size on flow layout
        flowLayout.itemSize = CGSize(width: itemWidthAndHeight, height: itemWidthAndHeight)
        // Set section insets on flow layout
        flowLayout.sectionInset = UIEdgeInsetsMake(sectionInset, sectionInset, sectionInset, sectionInset)
        // Set spacings on flow layout
        flowLayout.minimumInteritemSpacing = cellSpacing
        flowLayout.minimumLineSpacing = cellSpacing
        return flowLayout
    }
    
    private func makeSegmentedControlView() -> SegmentedControlView {
        let segmentedControlView = SegmentedControlView()
        segmentedControlView.translatesAutoresizingMaskIntoConstraints = false
        segmentedControlView.selectItem(at: 0)
        segmentedControlView.delegate = self
        return segmentedControlView
    }
    
}

// MARK: UICollectionViewDelegate

extension HousesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let house = presenter.house(at: indexPath.item) else { return }
        selectedHouse?(house, presenter)
    }
    
}

// MARK: - SegmentedControlViewDelegate

extension HousesViewController: SegmentedControlViewDelegate {
    
    func didSelectCategory(_ category: HouseCategory) {
        reloadCollectionView(category: category)
    }
    
}
