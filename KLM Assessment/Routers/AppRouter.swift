//
//  AppRouter.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class AppRouter {
    
    // MARK: - Private properties
    
    private let window: UIWindow
    private var currentRouter: KGRouter?
    
    // MARK: - Initializers
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - Public methods
    
    func startApplication() {
        currentRouter = HousesRouter()
        window.rootViewController = currentRouter?.makeFirstViewController()
        window.makeKeyAndVisible()
    }
    
}
