//
//  HousesRouter.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class HousesRouter: KGRouter {
    
    // MARK: - Private properties
    
    private var navigationController: UINavigationController?
    
    // MARK: - Public methods
    
    func makeFirstViewController() -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let housesViewController = storyboard.instantiateViewController(withIdentifier: "HousesViewController") as? HousesViewController else {
            assertionFailure("Can not create HousesViewController")
            return nil
        }
        let presenter = HousesViewPresenter(interactor: HouseInteractor())
        housesViewController.presenter = presenter
        
        housesViewController.selectedHouse = { house, presenter in
            self.navigateToHouseDetail(selectedHouse: house, presenter: presenter)
        }
        navigationController = KGNavigationController(rootViewController: housesViewController)
        return navigationController
    }
    
    // MARK: - Private methods
    
    private func navigateToHouseDetail(selectedHouse: House, presenter: HousesViewPresenter) {
        presenter.filterType = .all
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let selectedIndex = presenter.index(of: selectedHouse),
              let housesPagingViewController = storyboard.instantiateViewController(withIdentifier: "HousesPagingViewController") as? HousesPagingViewController else {
                assertionFailure("Can not create HousesPagingViewController")
                return
        }
        housesPagingViewController.selectedIndex = selectedIndex
        housesPagingViewController.presenter = presenter
        navigationController?.pushViewController(housesPagingViewController, animated: true)
    }
    
}
