//
//  HouseInteractor.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

class HouseInteractor {
    
    // MARK: - Public properties
    
    var numberOfHouses: Int {
        return filteredHouses.count
    }
    var filterType: HouseCategory = .all
    
    // MARK: - Private properties
    
    private var filteredHouses: [House] {
        switch filterType {
        case .collected:
            return houses.filter { $0.amount > 0 }
        case .duplicate:
            return houses.filter { $0.amount > 1 }
        case .missing:
            return houses.filter { $0.amount == 0 }
        case .all:
            return houses
        }
    }
    private lazy var houses: [House] = {
        guard let houseJsonArray: [[String: Any]] = JSONParser.jsonObjectFrom(fileName: "houses") else {
            return []
        }
        return houseJsonArray.flatMap { House(json: $0) }.sorted(by: { $0.number > $1.number })
    }()
    
    // MARK: - Public methods
    
    func house(at index: Int) -> House? {
        guard index >= 0 && index < filteredHouses.count else { return nil }
        return filteredHouses[index]
    }
    
    func index(of house: House) -> Int? {
        return filteredHouses.index { $0.identifier == house.identifier }
    }
    
}
