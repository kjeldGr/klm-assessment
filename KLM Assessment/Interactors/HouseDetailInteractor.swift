//
//  HouseDetailInteractor.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

class HouseDetailInteractor {
    
    private let house: House
    
    init(house: House) {
        self.house = house
    }
    
}
