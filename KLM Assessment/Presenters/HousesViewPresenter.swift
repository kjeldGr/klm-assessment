//
//  HousesViewPresenter.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import Foundation

class HousesViewPresenter {
    
    // MARK: - Public properties
    
    var filterType: HouseCategory {
        get {
            return interactor.filterType
        }
        set {
            interactor.filterType = newValue
        }
    }
    var numberOfHouses: Int {
        return interactor.numberOfHouses
    }
    
    // MARK: - Private properties
    
    private let interactor: HouseInteractor
    
    // MARK: - Initializers
    
    init(interactor: HouseInteractor) {
        self.interactor = interactor
    }
    
    // MARK: - Public methods
    
    func house(at index: Int) -> House? {
        return interactor.house(at: index)
    }
    
    func index(of house: House) -> Int? {
        return interactor.index(of: house)
    }
    
}
