//
//  HouseDetailViewPresenter.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class HouseDetailViewPresenter {
    
    // MARK: - Public properties
    
    var displayDescription: String {
        return house.description
    }
    var displayName: String {
        return house.name
    }
    var displayAddress: String {
        return house.address
    }
    var displayNumber: String {
        return "\(house.number)"
    }
    var displayImage: UIImage? {
        return house.image
    }
    
    // MARK: - Private properties
    
    private let house: House
    
    // MARK: - Initializers
    
    init(house: House) {
        self.house = house
    }
    
    
}
