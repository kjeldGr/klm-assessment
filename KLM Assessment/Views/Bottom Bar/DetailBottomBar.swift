//
//  DetailBottomBar.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

protocol DetailBottomBarDelegate: class {
    func infoButtonPressed()
}

class DetailBottomBar: BottomBar {

    // MARK: - Public properties
    
    weak var delegate: DetailBottomBarDelegate?
    
    // MARK: - Private properties
    
    fileprivate var housePresentable: HousePresentable?
    private lazy var amountButton: UIButton = self.makeAmountButton()
    private lazy var infoButton: UIButton = self.makeInfoButton()
    fileprivate lazy var changeAmountBar: ChangeAmountBottomBar = self.makeChangeAmountBar()
    
    // MARK: - Public methods
    
    override func setupView() {
        super.setupView()
        setupAmountButton()
        setupInfoButton()
        setupChangeAmountBar()
    }
    
    func layout(with housePresentable: HousePresentable) {
        self.housePresentable = housePresentable
        reloadLayout()
    }
    
    // MARK: - Private methods
    
    fileprivate func reloadLayout() {
        guard let housePresentable = self.housePresentable else { return }
        amountButton.setTitle("\(housePresentable.amount) x", for: .normal)
        changeAmountBar.amount = housePresentable.amount
    }
    
    @objc private func infoButtonPressed(_ sender: Any) {
        delegate?.infoButtonPressed()
    }
    
    @objc private func amountButtonPressed(_ sender: Any) {
        changeAmountBar.isHidden = false
    }
    
    // MARK: UI methods
    
    private func setupAmountButton() {
        addSubview(amountButton)
        addConstraint(NSLayoutConstraint(item: amountButton, attribute: .left, relatedBy: .equal,
                                         toItem: self, attribute: .left, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: amountButton, attribute: .centerY, relatedBy: .equal,
                                         toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    private func setupInfoButton() {
        addSubview(infoButton)
        addConstraint(NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal,
                                         toItem: infoButton, attribute: .right, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: infoButton, attribute: .centerY, relatedBy: .equal,
                                         toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    private func setupChangeAmountBar() {
        addSubview(changeAmountBar)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[changeAmountBar]-0-|", options: [], metrics: nil, views: ["changeAmountBar": changeAmountBar]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[changeAmountBar]-0-|", options: [], metrics: nil, views: ["changeAmountBar": changeAmountBar]))
    }
    
    // MARK: Factory methods
    
    private func makeAmountButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(amountButtonPressed), for: .touchUpInside)
        return button
    }
    
    private func makeInfoButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "InfoIcon"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(infoButtonPressed), for: .touchUpInside)
        return button
    }
    
    private func makeChangeAmountBar() -> ChangeAmountBottomBar {
        let changeAmountBottomBar = ChangeAmountBottomBar()
        changeAmountBottomBar.translatesAutoresizingMaskIntoConstraints = false
        changeAmountBottomBar.delegate = self
        changeAmountBottomBar.isHidden = true
        return changeAmountBottomBar
    }
    
}

extension DetailBottomBar: ChangeAmountBottomBarDelegate {
    
    func increaseAmount() {
        housePresentable?.increaseAmount()
        reloadLayout()
    }
    
    func decreaseAmount() {
        housePresentable?.decreaseAmount()
        reloadLayout()
    }
    
    func hideChangeAmountView() {
        changeAmountBar.isHidden = true
    }
    
}
