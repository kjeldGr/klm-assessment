//
//  SegmentedControlView.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

enum HouseCategory: String {
    case all = "category_title_all"
    case missing = "category_title_missing"
    case collected = "category_title_collected"
    case duplicate = "category_title_duplicate"
    
    static var allCategories: [HouseCategory] {
        return [.all, .missing, .collected, .duplicate]
    }
}

protocol SegmentedControlViewDelegate: class {
    func didSelectCategory(_ category: HouseCategory)
}

class SegmentedControlView: BottomBar {
    
    // MARK: - Public properties
    
    weak var delegate: SegmentedControlViewDelegate?
    
    // MARK: - Private properties
    
    private lazy var segmentedControl: UISegmentedControl = self.makeSegmentedControl()
    
    // MARK: - Public methods
    
    override func setupView() {
        super.setupView()
        setupSegmentedControl()
    }
    
    func selectItem(at index: Int) {
        segmentedControl.selectedSegmentIndex = index
    }
    
    // MARK: - Private methods
    
    @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        let category = HouseCategory.allCategories[sender.selectedSegmentIndex]
        delegate?.didSelectCategory(category)
    }
    
    // MARK: UI methods
    
    private func setupSegmentedControl() {
        addSubview(segmentedControl)
        addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .centerX, relatedBy: .equal,
                                         toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .centerY, relatedBy: .equal,
                                         toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    // MARK: Factory methods
    
    private func makeSegmentedControl() -> UISegmentedControl {
        let segmentedItems = HouseCategory.allCategories.map { String.localize($0.rawValue) }
        let segmentedControl = UISegmentedControl(items: segmentedItems)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.tintColor = .white
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
        return segmentedControl
    }
    
}
