//
//  ChangeAmountBottomBar.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 03-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

protocol ChangeAmountBottomBarDelegate: class {
    func increaseAmount()
    func decreaseAmount()
    func hideChangeAmountView()
}

class ChangeAmountBottomBar: BottomBar {

    // MARK: - Public properties
    
    weak var delegate: ChangeAmountBottomBarDelegate?
    var amount: Int! {
        didSet {
            amountLabel.text = "\(amount!) x"
        }
    }
    
    // MARK: - Private properties
    
    private lazy var plusButton: UIButton = self.makePlusButton()
    private lazy var minusButton: UIButton = self.makeMinusButton()
    private lazy var amountLabel: UILabel = self.makeAmountLabel()
    private lazy var cancelButton: UIButton = self.makeCancelButton()
    
    // MARK: - Private methods
    
    @objc private func plusButtonPressed(_ sender: Any) {
        delegate?.increaseAmount()
    }
    
    @objc private func minusButtonPressed(_ sender: Any) {
        delegate?.decreaseAmount()
    }
    
    @objc private func cancelButtonPressed(_ sender: Any) {
        delegate?.hideChangeAmountView()
    }
    
    // MARK: UI methods
    
    override func setupView() {
        super.setupView()
        setupMinusButton()
        setupPlusButton()
        setupAmountLabel()
        setupCancelButton()
    }
    
    private func setupMinusButton() {
        addSubview(minusButton)
        addConstraint(NSLayoutConstraint(item: minusButton, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: minusButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    private func setupPlusButton() {
        addSubview(plusButton)
        addConstraint(NSLayoutConstraint(item: plusButton, attribute: .left, relatedBy: .equal, toItem: minusButton, attribute: .right, multiplier: 1, constant: 5))
        addConstraint(NSLayoutConstraint(item: plusButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    private func setupAmountLabel() {
        addSubview(amountLabel)
        addConstraint(NSLayoutConstraint(item: amountLabel, attribute: .left, relatedBy: .equal, toItem: plusButton, attribute: .right, multiplier: 1, constant: 5))
        addConstraint(NSLayoutConstraint(item: amountLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    private func setupCancelButton() {
        addSubview(cancelButton)
        addConstraint(NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: cancelButton, attribute: .right, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: cancelButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    // MARK: Factory methods
    
    private func makePlusButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("+", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(plusButtonPressed), for: .touchUpInside)
        return button
    }
    
    private func makeMinusButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("-", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(minusButtonPressed), for: .touchUpInside)
        return button
    }
    
    private func makeAmountLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        return label
    }
    
    private func makeCancelButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Hide", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        return button
    }

}
