//
//  HouseCell.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class HouseCell: UICollectionViewCell, ReusableView {
    
    // MARK: - Public properties
    
    lazy var imageView: UIImageView = self.makeImageView()
    
    // MARK: - Private properties
    
    private lazy var doneView: UIView = self.makeDoneView()
    
    // MARK: - Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Public methods
    
    func setupView() {
        setupImageView()
        setupDoneView()
    }
    
    func present(with presentable: HousePresentable) {
        imageView.image = presentable.image
        doneView.isHidden = presentable.amount == 0
    }
    
    // MARK: - Private methods
    
    private func setupImageView() {
        contentView.addSubview(imageView)
        contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[imageView]-0-|", options: [], metrics: nil,
            views: ["imageView": imageView]))
        contentView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .left, relatedBy: .equal,
                                                     toItem: contentView, attribute: .left, multiplier: 1,
                                                     constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal,
                                                     toItem: imageView, attribute: .height, multiplier: 1,
                                                     constant: 0))
    }
    
    private func setupDoneView() {
        imageView.addSubview(doneView)
        imageView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[doneView]-0-|", options: [], metrics: nil, views: ["doneView": doneView]))
        imageView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[doneView]-0-|", options: [], metrics: nil, views: ["doneView": doneView]))
    }
    
    // MARK: Factory methods
    
    private func makeImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = AppDesign.color(.border).cgColor
        return imageView
    }
    
    private func makeDoneView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view.isHidden = true
        let circleSize = CGFloat(26)
        let circleView = UIView()
        circleView.translatesAutoresizingMaskIntoConstraints = false
        circleView.backgroundColor = .green
        circleView.layer.cornerRadius = circleSize / 2
        view.addSubview(circleView)
        view.addConstraint(NSLayoutConstraint(item: circleView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: circleView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: circleView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: circleSize))
        view.addConstraint(NSLayoutConstraint(item: circleView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: circleSize))
        return view
    }
    
}
