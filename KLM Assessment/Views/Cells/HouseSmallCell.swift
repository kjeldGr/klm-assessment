//
//  HouseSmallCell.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class HouseSmallCell: HouseCell {
    
    private lazy var numberLabel: UILabel = self.makeNumberLabel()
    
    override func setupView() {
        super.setupView()
        setupNumberLabel()
    }
    
    override func present(with presentable: HousePresentable) {
        super.present(with: presentable)
        numberLabel.text = "\(presentable.number)"
    }
    
    // MARK: - Private methods
    
    private func setupNumberLabel() {
        imageView.addSubview(numberLabel)
        imageView.addConstraint(NSLayoutConstraint(
            item: numberLabel, attribute: .top, relatedBy: .equal, toItem: imageView, attribute: .top,
            multiplier: 1, constant: 5))
        imageView.addConstraint(NSLayoutConstraint(
            item: numberLabel, attribute: .left, relatedBy: .equal, toItem: imageView, attribute: .left,
            multiplier: 1, constant: 5))
    }
    
    // MARK: Factory methods
    
    private func makeNumberLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = AppDesign.color(.houseNumber)
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }
    
}
