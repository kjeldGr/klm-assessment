//
//  House.swift
//  KLM Assessment
//
//  Created by Kjeld Groot on 02-10-17.
//  Copyright © 2017 KPGroot. All rights reserved.
//

import UIKit

class House {
    
    let identifier: Int
    let name: String
    let number: Int
    let address: String
    let imageUrls: [String]
    let description: String
    var images: [UIImage] {
        return imageUrls.flatMap { UIImage(named: $0) }
    }
    var amount: Int = 0
    
    init?(json: [String: Any]) {
        guard let identifier = json["id"] as? Int,
              let name = json["name"] as? String,
              let number = json["number"] as? Int,
              let address = json["address"] as? String,
              let description = json["description"] as? String,
              let imageUrls = json["images"] as? [String] else {
                return nil
        }
        self.identifier = identifier
        self.name = name
        self.number = number
        self.address = address
        self.description = description
        self.imageUrls = imageUrls
    }
    
}

extension House: HousePresentable {
    
    var displayName: String {
        return name
    }
    var image: UIImage? {
        return images.first
    }
    var detailImages: [UIImage]? {
        guard images.count > 1 else { return nil }
        return Array(images.suffix(from: 1))
    }
    
    func increaseAmount() {
        amount += 1
    }
    
    func decreaseAmount() {
        amount = amount - 1 < 0 ? 0 : amount - 1
    }
    
}
